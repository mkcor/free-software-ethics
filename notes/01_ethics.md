## Ethics vs. Morality

I assume (I hope!) that many of you use free software. Perhaps some of you even
develop free software. And I guess that you do so for ethical reasons. And that
is great, by the way. If we are not well-versed in philosophy, we may use the
words ‘ethical’ and ‘moral’ interchangeably. But there is actually a difference
between ethics and morality, in philosophy.

In the first part of this talk (after this long introduction), I will explain
this difference, and how it relates to the GNU philosophy. The second (and
last) part will be about the philosophical movements and social movements,
which we can relate to the GNU philosophy. So what is ethics? We will start in
ancient Greece with Aristotle (384--322 BC), who is behind the
“Nicomachean Ethics”---which is a reference work that I have not read.

Aristotle investigates the question of how to live best. What is good living?
He is interested in real life, real objects, and real systems. As much as
we can learn from contemplative philosophy, he wants to bring its findings
down to earth and see them in use. Ethics is the answer (to the question of
good living) for the individual. It is practical, not theoretical. It is a way
of life.

Aristotle explains that, to live best, you should avoid deficiency or excess.
How to be a charming guest, for example? Don’t be completely quiet and
withdrawn, but don’t act like a clown either. So really there is nothing moral
there. Ethics is about finding thresholds in human conduct that keep us from
ruining our life, ruining our friendships, our partnerships, our
collaborations.

Ethics gives you a procedure to make the most of a given situation. You have to
be careful, before making choices and taking steps. Ethics comes into play
because you care (you are not careless), you consider the situation carefully.
But, again, the goal is to be effective. The approach is pragmatic. There is no
reference whatsoever to, say, respect for others.

Other ancient philosophers have contributed other versions of ethics. Maybe you
have heard of the Stoics. Maybe you have read the “Meditations” of Marcus
Aurelius (121--180). I only have anecdata, but I am under the impression that
it is a popular read in our communities... Anyway, I personally enjoy reading a
few *meditations* (by Marcus Aurelius) every now and then.

On a side note, I have noticed that certain people, when we refer to Stoicism
and only mention or quote Marcus Aurelius, they like to clarify that he was
not the founding father of Stoicism, that he did not come up with these ideas
himself, he did not invent them. Well, sure. I haven’t invented anything
either! We stand on the shoulders of giants. And we play with building blocks
like children do. We hack the existing.

Plus, some of us have day jobs. Marcus Aurelius was running the Roman
Empire, after all! Okay, I’m getting side-tracked. I guess the emphasis---felt
as an obsession---about who *invented* what *first* triggers me, because the
very myth around this notion of *invention* is used to justify the abusive
patent system that we know. But this is not today’s topic. I gave a
[rant](https://framagit.org/mkcor/fsm-2016/blob/master/notes/02_brevets.txt)
against software patents at the World Social Forum, last summer.
And, yesterday, Deb Nicholson talked about
[patent creeps](https://media.libreplanet.org/u/libreplanet/m/patents-copyrights-and-trademarks-won-t-someone-please-think-of-the-children/).

Marcus Aurelius tries to have the best behaviour with his friends, with his
colleagues, with himself, so as not to harm himself. Again, it is operational.
It is not moral. Ethics, really, is a cost--benefit analysis. Does this sound
familiar? I don’t know about you (I hope you are still with me), but this
reminds me very much of the philosophy of the open source movement.

If you isolate the definition of free software, you have these four essential
freedoms... which are extremely convenient! You have the freedom to run the
program as you wish (freedom 0). This could come in handy. You have the freedom
to study how the program works, and change it so it does your computing as you
wish (freedom 1). That’s even better. I could go on with freedoms 2 and 3.

There is certainly nothing wrong with this approach---enjoying the convenience.
But there is nothing right either! Because it’s not about right or wrong---in
the moral sense. We could talk about the ethics of open source. For example,
by going open source, you can get upstream contributions! RMS pointed this out
during Brad’s
[talk](https://media.libreplanet.org/u/libreplanet/m/patents-copyrights-and-trademarks-won-t-someone-please-think-of-the-children/)
a little earlier. It’s about practicality and convenience. On the other hand,
the ethics of free software go beyond these operational aspects---hence the
session title (“Ethics beyond ethics”).

To wrap up the historical philosophical exposition of ethics---this exposition
being by no means comprehensive, I will mention “Ethics, Demonstrated in
Geometrical Order” by Spinoza (1632--1677), which is the last treatise on
ethics. It is a masterpiece. And I have not read it. For our purposes, I will
only say that Spinoza derives propositions, math style, as the title suggests.
It’s about optimization, it’s about decision-making.

What is the risk, if I choose option A over option B? Spinoza identifies active
passions vs. passive passions. And he recommends that we indulge in active
passions rather than passive passions. Active passions are pleasurable and
create a positive feedback loop, while passive passions are painful and create
a negative feedback loop. Ethics help us balance alternatives and optimize
outcomes. Considerations about autonomy, about respect do not come into play.

The philosophical jump occurs in the following century, with Jean-Jacques
Rousseau (1712--1778). Paving the way for the Age of Enlightenment, Rousseau
leaves risk calculation behind, and introduces notions such as pity (*pitié*)
and sympathy (*sympathie*). Meanings have shifted meanwhile, so I believe the
meaning of his ‘pity’ would be compassion nowadays, and the meaning of his
‘sympathy’ would be *empathy*: It is the ability to feel what somebody else is
feeling (not just the ability to care about it).

Here comes morality! Finally. Let me move on to the second part of the talk,
about humanism and derived movements.
