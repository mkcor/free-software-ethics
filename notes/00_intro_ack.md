# The GNU philosophy: Ethics beyond ethics

Hello, World!

## Introduction and Acknowledgments

First of all, I want to thank you for your interest in this session, a session
about philosophy. I also want to thank the LibrePlanet organizers for having me
as a speaker today. My name is Marianne Corvellec. Some of you may have seen me
before, for I spoke at the past two editions of LibrePlanet. Back then,
I presented alongside Jonathan Le Lous, on very different topics.
It is my pleasure to be back!

I will not be using slides during this talk. I will try to speak very clearly.
But if, for any reason, you do not catch a name or a reference that I mention
(especially when I pronounce French names in French), do not fret: Later,
I will publish the text of my speech under a Creative Commons license,
in a public repository hosted on an instance of GitLab.

Before we dive into the GNU philosophy, I will introduce myself and my
organization. I will also provide some background and context for this
presentation. Personally, I work at a computer science research centre called
[CRIM](https://crim.ca) in Montréal, Québec. I joined as a data scientist
almost a year ago.

Basically, I am not a professional philosopher! Likewise, I was not a lawyer
when I discussed free-trade agreements two years ago. You can take this as
a disclaimer or as an invitation not to be intimated---philosophy can be
awe-inspiring, right? But this detail also happens to tie in with Renaissance
humanism, as in Renaissance man vs. expert.

Nowadays, experts are very gratified, very sought after. This is exemplified
by the “priesthood of technology”, which keeps the general public in ignorance
of how technology works (quoting an article of the GNU philosophy, actually
found under [Education](https://www.gnu.org/education/edu-schools.en.html)).
The free software movement denounces and rejects this “priesthood of
technology”.

I wish for a new Renaissance! I am excited to spark conversations during and
after this session, online and offline. Hopefully we can grow our own knowledge
and foster the encyclopaedic style... Like many of you in this room, I am a
free software activist. More formally, I am a board member with
[April](https://april.org)---spelled like the month of April.

April is a French not-for-profit advocating free software (in France and
Europe). A few months ago, we celebrated its 20 years of existence. We have
worked hand in hand with the FSF since the beginning (this is a collective
‘we’). Currently, we have a little over 4,000 members (90% of which are people,
the remainder being businesses, organizations, research entities, and local
communities).

We have three permanent staff and many joyful volunteers, who work on outreach
projects as well as policy matters (much like the FSF). I joined April in 2011.
I was motivated by freedom issues, which I encountered in an academic setting
(I was a PhD student in theoretical and computational physics). I have a sort
of personal equivalent to Richard Stallman’s story with the Xerox printer!

I will not share it now though, because I am already digressing a lot. I shared
[it](https://framagit.org/mkcor/fsm-2016/blob/master/notes/01_emancipation.txt)
last summer, at the World Social Forum---the 2016 edition took place in
Montréal. I spoke at the free software conference there, after RMS. And my
notes are online (but they are in French). Back in France (which is where I
grew up), I was very active in the field.

I used to organize a local group of April members and allies (in Lyon, France).
This dates back to 2012. That same year---you might guess that I was in between
jobs (and I was, and I want to say it out loud), I campaigned against the
European project of a unitary patent system. Otherwise,
I have contributed initiatives for the promotion and adoption of free software
in the context of scientific research and higher education.

This, I still do. I have kept or developed a deep connection with some academic
communities. Notably, I volunteer with the Carpentries (collective term for
[Software Carpentry](https://software-carpentry.org/),
[Data Carpentry](http://www.datacarpentry.org/), and more coming).
When I returned to Québec in 2013, I naturally got in touch with
[Facil](https://facil.qc.ca/), the not-for-profit organization focused on
advancing free software in the province.

I definitely support them---I am an ally, but I am not active with them.
I know that, locally, we also have an [APC](https://www.apc.org/)
presence---that is the Association for Progressive Communications.
At the World Social Forum alone, I noticed that we could (and should) have
joined forces, in order to be more visible and effective. Wait... this just
sounded very result-oriented, didn’t it?

Quite like or unlike the ethics of free software? So let’s talk about it.
I will start by giving a definition of ethics, which comes from a philosophical
and historical standpoint. Now, I must credit Véronique Bonnet, who sits
with me on the board of April. *She* is advancedly trained in philosophy, and
teaches philosophy at the university level (in France). This presentation
draws heavily on contributions by Véronique Bonnet.

Finally, I want to thank my dear friends and family, for all the inspiring
and insightful conversations. Their knowledge, their intuition, and their brain
power have added tremendously to the present contribution. I also want to
express my gratitude for the logistical and emotional support they have offered
me. They know who they are---thank you so much.
