## Humanism (and Derived Works)

Bootstrapping the Enlightenment, Jean-Jacques Rousseau and Immanuel Kant
(1724--1804) fork traditional ethics, coming up with morality. Morality is not
about being smart or playing smart (assuming that if I am start, I will see
clearly, and I will know what to do). Morality is about listening to your inner
voice, which tells you what your obligation is, and acting accordingly.

We are in the 18th century and, after all the Cartesianism, we are turning the
spotlight on sensitivity, subjectivity, and what will lead to Romanticism. The
human being is a subject, not an object. This is the humanist perspective. What
is humanism? I hear that it’s about putting the human being front and centre...
Great. But I’m not sure what this entails exactly.

I visualize the [“Vitruvian Man”](https://en.wikipedia.org/wiki/Vitruvian_Man)
by Leonardo da Vinci, this drawing with annotations. We can see a human body
at the centre of a circle and a square, so it means the human being is central,
he is the centre of the Universe? I find that the key (the explicit version?)
is given by Rousseau and Kant.

Humanism is the position that, deep down, we humans are all the same. We are
fellow creatures, we are equals. And we equals are meant to form a community,
we are meant to talk to each other, and we are meant to share. We are meant to
learn, we are meant to experience by ourselves (which will later lead to...
phenomenology?).

In the [GNU Manifesto](https://www.gnu.org/gnu/manifesto.en.html), we find many
phrases which belong to the lexicon of moral obligation (‘I must’, ‘I cannot in
good conscience...’), as well as expressions of the non-negotiable principle of
reciprocity (‘I refuse to break solidarity with other users’). RMS used to work
at the Artificial Intelligence Lab here at MIT, and sharing had always been at
the heart of academic practices.

Intellectual exchange is how we advance knowledge and, hence, add to our
intangible heritage. The GNU Project is one of the first examples of digital
commons. Wikipedia is another well-known example of digital commons. Naturally,
we are in a coalition together. Sue Gardner, former Executive Director of the
Wikimedia Foundation, was a keynote speaker at LibrePlanet 2014.

Speaking of intangible heritage, I recently came across the definition of
Intangible Cultural Heritage
([ICH](http://www.unesco.org/culture/ich/en/what-is-intangible-heritage-00003))
by UNESCO. And this was in a technological context of motion capturing and
modelling (at my work). Consider this other kind of commons: Cultural
expression is not limited to architecture, monuments, collection of objects, or
music. It also includes fragile intangible live expressions, which involve
knowledge and skills. For example, human gestures in traditional craftsmanship.

I will link to a
[paper](https://hal-mines-paristech.archives-ouvertes.fr/hal-00975857v2/document)
which focuses on the art of pottery. When the mastery of these gestures is only
held by a few people who are very old, this knowledge is at risk of extinction.
I do not know if the project is still active or funded or anything, but the
paper refers to a multidisciplinary research project called “ArtiMuse”. Their
mission is to develop and apply gesture recognition methodologies in musical
and handicraft interactions, in order to document, store, and safeguard these
rare gestural skills.

So maybe there is a potential coalition here... Anyway, I thought it could be
of interest to our friends in technical fields, in artistic fields, and to all
the librarians, of course! We are already friends and allies with librarians
who stand for freedom (and especially free speech of marginalized people). The
[Library Freedom Project](https://libraryfreedomproject.org/) presented a
session at LibrePlanet 2015, and received the Free Software award for Projects
of Social Benefit at LibrePlanet 2016.

We share the goal of “[making] our society better”, by “spreading freedom and
cooperation.” I quoted words from another article in the GNU philosophy, which
is [Copyleft: Pragmatic Idealism](https://www.gnu.org/philosophy/pragmatic.html.en).
Towards the end of the article, you can read sentences such as “Pragmatically
speaking, thinking about greater long-term goals will strengthen your will to
resist this pressure”---the pressure in question being that of “catering to
proprietary software.”

That sounds so much like ethics in the Antiquity. The same goes for “The
temptation can be powerful, but in the long run we are all better off if we
resist it.” But this ethics is not just ethics (in the pre-Enlightenment
sense), because it is rooted in values, which come first (see the beginning of
the article). Pragmatically, we make use of copyleft licensing to induce
freedom.

Likewise, in the 19th century, the Transcendentalists worked for freedom not
only with their intellectual debates, but by active participation in social
movements, notably abolitionism and feminism. So I read “Self-Reliance”, the
essay by Waldo Emerson (1803--1882), who started the Transcendental Club with
a couple of other New England intellectuals. Maybe you know of self-reliance
and Emerson in the context of American libertarian ideas---I have not prepared
a formal paragraph about this, but we can talk about it during the Q&A.

So Emerson promotes independence, autonomy, self-trust, exploration... I find
it very comforting for people like us (at least like me, like hackers) who feel
different more often than not. One quote, for example: “Whoso would be a man,
must be a nonconformist.” The goal is to become fully human, a goal we have not
achieved yet. So it’s a humanism---an individualistic one. So how do we form a
community?

We need a social contract (Rousseau). And to have a social contract, we need
empathy in the first place. It’s not enough to have something technical that
works well. It’s not about technicality. You might think that empathy is not
exactly in line with individualist anarchism (or the thought of Emerson), but I
think it is actually far from incompatible. By becoming fully human and fully
yourself, you reach something that is universal, that all humans have in
common. All spirituality is like that---counterintuitive at first glance.

In yet another article of the GNU
philosophy, which is titled
[Free Software Is Even More Important Now](https://www.gnu.org/philosophy/free-software-even-more-important.en.html),
RMS explains how unacceptable it is to “mistreat the users”, to control them,
to deprive them of their freedom. Quote: “Freedom includes the freedom to
cooperate with others. Denying people that freedom means keeping them divided,
which is the start of a scheme to oppress them.” Unquote. There is a very
existential (almost Sartrian, as Véronique Bonnet points out) sentence too:
“Life without freedom is oppression, and that applies to computing as well as
every other activity in our lives.”

Existentialism is a humanism developed in the 20th century. I have not read
Jean-Paul Sartre (1905--1980), so I will not elaborate. Briefly, existentialism
posits that we are always responsible so, in a way, we are always free. And I
find that the free software movement embodies this so well, because it is our
responsibility to resist, say, catering to proprietary software. We can choose
free software for our computing life.

A little earlier, I uttered the word ‘anarchism’---I still need to do my
homework on this topic. But the fact is, anarchism is the school of thought at
the root of freedom. Anarchism is about challenging power, domination,
oppression. By definition, it is at the root (at least in inspiration) of any
movement for social justice. I would imagine that we should be in a coalition
with global movements such as Occupy. On my way to the conference, I was
reading a novella by Cory Doctorow, who gave the keynote this morning. It’s
called “Lawful Interception” and, in it, the main character participates in
Occupy Oakland. But this is a work of fiction, so...

I would be happy to learn from the audience, and open the floor to questions or
comments. Thank you!
