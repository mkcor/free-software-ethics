# LibrePlanet 2017

I presented a session on the free software movement and the philosophy of
ethics at [LibrePlanet 2017](https://libreplanet.org/2017/program/).

I spoke for [April](https://april.org/).

* Read the [notes](notes/).
* Watch the [video](https://media.libreplanet.org/u/libreplanet/m/the-gnu-philosophy-ethics-beyond-ethics/).

[Marianne Corvellec](https://libreplanet.org/2017/program/speakers.html#corvellec)

## The GNU philosophy: Ethics beyond ethics

Ethics is at the root of Free Software. In a philosophical perspective where
ethics is operational rather than moral, we argue that the ethics of Free
Software goes beyond ethics. It is morality. The ever-present concern for
self-respect, autonomy and, of course, freedom makes Free Software akin to
historical philosophical movements (humanism, Enlightenment, existentialism).
Besides, the Free Software Movement contribute their principles (such as
transparency) and practices (such as cryptography) to support whistleblowers,
journalists, and activists. Similarities with other social movements let us
derive existing and possible coalitions.

## Recommended

* [Free Software Awards](https://media.libreplanet.org/u/libreplanet/m/free-software-awards-with-richard-stallman-awards-presentation-and-speech/)
with Richard Stallman
* [The post-truth Santa Claus and the concealed present](https://media.libreplanet.org/u/libreplanet/m/the-post-truth-santa-claus-and-the-concealed-present/)
with Alexandre Oliva
* [SecureDrop: Leaking safely to modern news organizations](https://media.libreplanet.org/u/libreplanet/m/securedrop-leaking-safely-to-modern-news-organizations/)
with Conor Schaefer
